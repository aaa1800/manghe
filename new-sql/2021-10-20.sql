alter table box_setting
add column `retail_1` decimal(12,2) DEFAULT '0.000' COMMENT '分销一级充值占比（%）',
add column `retail_2` decimal(12,2) DEFAULT '0.000' COMMENT '分销二级充值占比（%）',
add column `retail_3` decimal(12,2) DEFAULT '0.000' COMMENT '分销三级充值占比（%）';

alter table box_setting
add column `kou` int(11) DEFAULT '0' COMMENT '扣量';

alter table box_user
add column `retail_1` decimal(12,2) DEFAULT '0.000' COMMENT '分销一级充值占比（%）',
add column `retail_2` decimal(12,2) DEFAULT '0.000' COMMENT '分销二级充值占比（%）',
add column `retail_3` decimal(12,2) DEFAULT '0.000' COMMENT '分销三级充值占比（%）';

CREATE TABLE `box_retail_invitation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户ID',
  `pid` int(10) unsigned DEFAULT NULL COMMENT '邀请人ID',
  `level` int(2) unsigned DEFAULT NULL COMMENT '1/2/3级',
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
	KEY(`pid`),
	KEY(`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分销关系表';

CREATE TABLE `box_retail_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) unsigned NOT NULL COMMENT '给谁分佣',
  `source_user_id` int(11) unsigned NOT NULL COMMENT '分佣来自于谁',
  `level` int(10) unsigned NOT NULL COMMENT '几级',
  `coin` decimal(12,2) DEFAULT '0' COMMENT '分佣金额',
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分佣明细表';

alter table box_coin_record change type 
type enum('pay_box','recharge','from_balance','refund','pay_delivery','retail') 
COLLATE utf8mb4_unicode_ci NOT NULL 
COMMENT '变更类型:pay_box=支付盲盒,recharge=充值,from_balance=余额转入,refund=库存不足，支付返回,pay_delivery=支付运费,retail=分销佣金';

alter table box_user add column `retail_balance` decimal(12,2) default 0 comment '佣金余额';
