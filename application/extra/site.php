<?php

return array (
  'name' => '商城',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => '',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '',
  'mail_smtp_pass' => '',
  'mail_verify_type' => '2',
  'mail_from' => '',
);
