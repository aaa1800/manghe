<?php

return [
    'Id'               => 'ID',
    'Box_id'           => '盲盒ID',
    'Order_id'         => '盲盒购买订单ID',
    'Out_trade_no'     => '商户订单号',
    'User_id'          => '用户ID',
    'Goods_id'         => '奖品ID',
    'Goods_name'       => '奖品名称',
    'Goods_image'      => '奖品图片',
    'Goods_coin_price' => '奖品金币价值',
    'Goods_rmb_price'  => '奖品RMB价值',
    'Status'           => '奖品状态',
    'Status bag'       => '盒柜',
    'Status exchange'  => '已回收',
    'Status delivery'  => '申请发货',
    'Status received'  => '已收货',
    'Exchange_time'    => '回收时间',
    'Delivery_time'    => '申请发货时间',
    'Delivery_fee'     => '运费',
    'Create_time'      => '开箱时间',
    'Update_time'      => '更新时间',
    'Delete_time'      => '删除时间',
    'Zz_time'          => '转赠时间'
];
