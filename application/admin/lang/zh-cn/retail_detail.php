<?php

return [
    'Id'             => 'ID',
    'User_id'        => '给谁分佣',
    'Source_user_id' => '分佣来自于谁',
    'Level'          => '几级',
    'Coin'           => '分佣金额',
    'Create_time'    => '创建时间',
    'Userphone'      => '受益者',
    'Sourcephone'    => '充值人'
];
