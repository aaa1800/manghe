<?php

return [
    'Id'          => 'ID',
    'Name'        => '分类名称',
    'Weigh'       => '权重',
    'Create_time' => '创建时间',
    'Update_time' => '更新时间',
    'Delete_time' => '删除时间'
];
