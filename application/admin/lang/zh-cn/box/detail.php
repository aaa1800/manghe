<?php

return [
    'Id'          => 'ID',
    'Box_id'      => '盲盒ID',
    'Box_Name'    => '盲盒名称',
    'Goods_id'    => '商品ID',
    'Goods_name'  => '商品名称',
    'Goods_image' => '商品图片',
    'Rate'        => '概率(%)',
    'Create_time' => '创建时间',
    'Update_time' => '更新时间',
    'Delete_time' => '删除时间'
];
