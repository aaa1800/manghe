<?php

return [
    'Id'          => 'ID',
    'Title'       => '标题',
    'Desc'        => '说明',
    'Text'        => '文本内容',
    'Type'        => '文本类别',
    'Public'      => '是否公共配置,非公共配置加标识才可见',
    'Update_time' => '更新时间'
];
