<?php

return [
    'Id'                => 'ID',
    'Logo_image'        => 'LOGO图片',
    'One_rmb_to_coin_num'  => '1RMB兑换金币数量',
    'Recovery_discount' => '商品回收金额占比（%）',
    'hot_box_banner'    => '热门盲盒顶部图片',
    'cheap_box_banner'  => '低价盲盒顶部图片',
    'Service_number'    => '客服号码',
    'Service_qrcode'    => '客服二维码',
    'Update_time'       => '更新时间',
    'retail_1'       => '一级分销佣金占比（%）',
    'retail_2'       => '二级分销佣金占比（%）',
    'retail_3'       => '三级分销佣金占比（%）',
    'kou'       => '扣量，大于5单小于10单，随机扣',
];
