<?php

return [
    'Id'            => 'ID',
    'Place'         => '位置',
    'Place index'   => '首页',
    'Place other'   => '其他',
    'Image'         => '图片',
    'Box'           => '盲盒',
    'Link'          => '链接',
    'Word'          => '文本',
    'Type'          => '类别',
    'Type box'      => '盲盒',
    'Type link'     => '链接',
    'Type word'     => '文本',
    'Value'         => '值',
    'Status'        => '状态',
    'Status normal' => '正常',
    'Status hidden' => '隐藏',
    'Weigh'         => '权重',
    'Update_time'   => '更新时间'
];
