<?php

return [
    'Id'               => 'ID',
    'Platform'         => '平台类别',
    'Platform h5'      => 'H5',
    'Platform android' => '安卓',
    'Platform ios'     => '苹果',
    'Platform applets' => '小程序',
    'Version'          => '版本号',
    'Content'          => '更新内容',
    'Download_file'    => '下载地址',
    'Enforce'          => '强制更新',
    'Enforce 1'        => '是',
    'Enforce 0'        => '否',
    'Create_time'      => '创建时间',
    'Update_time'      => '更新时间',
    'Delete_time'      => '删除时间'
];
