<?php

return [
    'Id'                   => 'ID',
    'User_id'              => '用户ID',
    'Before'               => '变更前',
    'After'                => '变更后',
    'Money'                => '变更金额',
    'Type'                 => '变更类型',
    'Type box_exchange'    => '盲盒回收',
    'Type refund'          => '库存不足',
    'Type withdrawal'      => '佣金提现',
    'Type to_coin'         => '转到钱包',
    'Type withdrawal_fail' => '提现失败',
    'Type withdrawals'     => '余额提现',
    'Order_id'             => '盲盒订单ID',
    'Prize_id'             => '奖品ID',
    'Create_time'          => '创建时间',
    'Update_time'          => '更新时间',
    'Delete_time'          => '删除时间'
];
