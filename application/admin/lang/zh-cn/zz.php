<?php

return [
    'Id'         => '序号',
    'Boxid'      => '盲盒ID',
    'Boxgoods'   => '盲盒商品',
    'Zzuser'     => '转赠人',
    'Szuser'     => '受赠人',
    'Zhuanztime' => '转赠时间',
    'Zzid'       => '转增ID'
];
