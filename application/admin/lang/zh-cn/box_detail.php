<?php

return [
    'Id'          => 'ID',
    'Box_id'      => '盲盒ID',
    'Goods_id'    => '商品ID',
    'Rate'        => '概率',
    'Weigh'       => '权重',
    'Create_time' => '创建时间',
    'Update_time' => '更新时间',
    'Delete_time' => '删除时间'
];
