<?php

return [
    'Id'          => 'ID',
    'Goods_name'  => '商品名称',
    'Tag'         => '标签',
    'Tag normal'  => '普通',
    'Tag rare'    => '稀有',
    'Tag supreme' => '史诗',
    'Tag legend' => '传说',
    'Tag zhizun' => '至尊',
    'Image'       => '商品图片',
    'Xiangqing'       => '详情图片',
    'Stock'       => '商品库存',
    'Coin_price'  => '金币价格',
    'Rmb_price'   => '人民币价格',
    'Delivery_fee'   => '运费',
    'Status'      => '状态',
    'Status online'    => '正常',
    'Status offline'   => '下架',
    'Create_time' => '创建时间',
    'Update_time' => '更新时间',
    'Delete_time' => '删除时间'
];
