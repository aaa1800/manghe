<?php

return [
    'Id'       => '编号',
    'Password' => '卡密',
    'Price'    => '金额',
    'User'     => '购买者',
    'Utime'    => '上传时间',
    'Etime'    => '核销时间',
    'Status'   => '状态',
    'Status 0' => '待核销',
    'Status 1' => '已核销'
];
