<?php

return [
    'Id'             => 'ID',
    'User_id'        => '用户ID',
    'User nickname'  => '用户昵称',
    'User avatar'    => '用户头像',
    'Amount'         => '金额',
    'Type'           => '提现方式',
    'Type wechat'    => '微信',
    'Type alipay'    => '支付宝',
    'Username'       => '真实姓名',
    'Account'        => '提现账号',
    'Status'         => '状态',
    'Status review'  => '审核中',
    'Set to success' => '设为通过',
    'Set to reject'  => '设为驳回',
    'Status success' => '通过',
    'Status reject'  => '已驳回',
    'Reason'         => '驳回原因',
    'Create_time'    => '申请时间',
    'Update_time'    => '审核时间',
    'Delete_time'    => '删除时间'
];
