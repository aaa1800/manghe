<?php

return [
    'Id'                => 'ID',
    'User_id'           => '用户ID',
    'User nickname'     => '用户昵称',
    'User mobile'       => '用户手机',
    'User avatar'       => '用户头像',
    'Coin_amount'       => '金币数量',
    'Rmb_amount'        => '人民币总价',
    'Pay_method'        => '支付方式',
    'Pay_method wechat' => '微信',
    'Pay_method alipay' => '支付宝',
    'Pay_rmb'           => '支付人民币',
    'Out_trade_no'      => '商户订单号',
    'Transaction_id'    => '微信平台订单号',
    'Pay_time'          => '付款时间',
    'Status'            => '状态',
    'Status unpay'      => '待支付',
    'Status paid'       => '已支付',
    'Backend_read'      => '后台已读标识:1已读,0未读',
    'Create_time'       => '创建时间',
    'Update_time'       => '更新时间',
    'Delete_time'       => '删除时间'
];
