<?php

return [
    'Id'                 => 'ID',
    'Order_id'           => '盲盒购买订单ID',
    'Out_trade_no'       => '购买盲盒订单号',
    'Delivery_order_no'  => '发货订单号',
    'Prize_id'           => '中奖ID',
    'Goods_name'         => '商品名称',
    'Goods_image'        => '商品图片',
    'User_id'            => '用户ID',
    'Nickname'           => '用户昵称',
    'avatar'             => '用户头像',
    'Username'           => '收货人姓名',
    'Mobile'             => '收货人电话',
    'Address'            => '收货地址',
    'Post_name'          => '快递公司名称',
    'Post_code'          => '快递公司代码',
    'Delivery_number'    => '快递单号',
    'Status'             => '订单状态',
    'Status undelivered' => '待发货',
    'Status unreceived'  => '待收货',
    'Status finished'    => '已完成',
    'Create_time'        => '申请时间',
    'Update_time'        => '更新时间',
    'Delete_time'        => '删除时间',
    'Delivery_time'      => '发货时间',
    'Receive_time'       => '收货时间',
];
