<?php

return [
    'Id'               => '中奖ID',
    'Order_id'         => '盲盒购买订单ID',
    'Out_trade_no'     => '商户订单号',
    'User_id'          => '用户ID',
    'Nickname'         => '用户昵称',
    'Avatar'           => '用户头像',
    'Mobile'           => '用户手机',
    'Goods_id'         => '奖品ID',
    'Goods_name'       => '商品名称',
    'Goods_image'      => '商品图片',
    'Goods_coin_price' => '商品金币价值',
    'Goods_rmb_price'  => '商品RMB价值',
    'Box_coin_price'   => '盲盒金币售价',
    'Box_rmb_price'    => '盲盒RMB售价',
    'Status'           => '奖品状态',
    'Status bag'       => '盒柜',
    'Status exchange'  => '已回收',
    'Status delivery'  => '申请发货',
    'Status received'  => '已收货',
    'Exchange_time'    => '回收时间',
    'Delivery_time'    => '申请发货时间',
    'Create_time'      => '开箱时间',
    'Update_time'      => '更新时间',
    'Delete_time'      => '删除时间',
    'Received_time'    => '收货时间'
];
