<?php

namespace app\admin\controller\trade;

use app\admin\model\Post;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 发货订单管理
 *
 * @icon fa fa-circle-o
 */
class Delivery extends Backend
{

    protected $noNeedRight = 'selectpage';

    /**
     * Delivery模型对象
     * @var \app\admin\model\trade\Delivery
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\trade\Delivery;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model->alias('delivery')
                ->field('delivery.id,delivery.out_trade_no,delivery.delivery_order_no,delivery.prize_id,delivery.goods_name,delivery.goods_image,delivery.username,delivery.mobile,delivery.address,delivery.post_name,delivery.post_code,delivery.delivery_number,delivery.status,delivery.create_time,delivery.delivery_time,delivery.receive_time')
                ->field('user.nickname,user.avatar,user.mobile')
                ->join('user user', 'user.id = delivery.user_id')
                ->where($where)
                ->where('delivery.status','neq','unpay')
                ->order($sort, $order)
                ->paginate($limit)
                ->each(function ($item) {
                    $item->avatar = $item->avatar ? cdnurl($item->avatar, true) : letter_avatar($item->nickname);
                });

            // 清空对应角标
            $filter = json_decode(input('filter'), true);
            if ($filter && !$list->isEmpty()) {
                \app\admin\model\trade\Delivery::where('backend_read', 0)->where('status', $list[0]->status)->update(['backend_read' => 1]);
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }

        // 渲染角标数字
        $sidebar = [
            'all' => \app\admin\model\trade\Delivery::where('backend_read', 0)->where('status','neq','unpay')->count(),
            'undelivered' => \app\admin\model\trade\Delivery::where('backend_read', 0)->where('status', 'undelivered')->count(),
            'unreceived' => \app\admin\model\trade\Delivery::where('backend_read', 0)->where('status', 'unreceived')->count(),
            'finished' => \app\admin\model\trade\Delivery::where('backend_read', 0)->where('status', 'finished')->count(),
        ];
        $this->assignconfig('sidebar_number', $sidebar);
        $this->assign('sidebar_number', $sidebar);
        return $this->view->fetch();
    }

    public function edit($ids = null)
    {
        $row = $this->model->alias('delivery')
            ->field('delivery.id,delivery.out_trade_no,delivery.prize_id,delivery.goods_name,delivery.goods_image,delivery.username,delivery.mobile,delivery.address,delivery.post_name,delivery.post_code,delivery.delivery_number,delivery.status,delivery.create_time')
            ->field('user.nickname,user.avatar,user.mobile')
            ->join('user user', 'user.id = delivery.user_id')
            ->where('delivery.id', $ids)
            ->find();

        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ('finished' == $row->status) {
                    $this->error('不能修改已完成订单');
                }

                if (!in_array($params['status'], ['undelivered', 'unreceived', 'finished'])) {
                    $this->error('订单状态有误');
                }

                if ('undelivered' == $row->status && 'unreceived' == $params['status']) {
                    $params['delivery_time'] = time();
                }

                if (empty($params['post_code'])) {
                    $this->error('快递公司名称不能为空');
                }
                if (empty($params['delivery_number'])) {
                    $this->error('快递单号不能为空');
                }

                // 检查快递公司
                $checkPostName = Post::where('post_code', $params['post_code'])->value('post_name');
                if (empty($checkPostName)) {
                    $this->error('快递公司名称有误');
                }
                $params['post_name'] = $checkPostName;

                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField('post_name,post_code,delivery_number,status,delivery_time')->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (\Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function selectpage()
    {
        $search = [];
        if (!empty($name = $this->request->request('q_word/a'))) {
            $search = ['post_name' => ['like', '%' . $name[0] . '%']];
        }

        $list = Post::field('post_name,post_code')->where($search)->select();

        $ret = [
            'list' => $list,
            'total' => count($list)
        ];

        return json($ret);
    }
}
