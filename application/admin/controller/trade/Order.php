<?php

namespace app\admin\controller\trade;

use app\admin\model\User;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 盲盒购买订单管理
 *
 * @icon fa fa-circle-o
 */
class Order extends Backend
{

    /**
     * Order模型对象
     * @var \app\admin\model\trade\Order
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\trade\Order;
        $this->view->assign("payMethodList", $this->model->getPayMethodList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model->alias('order')
                ->field('order.*')
                ->field('user.nickname,user.avatar')
                ->join('user user', 'user.id = order.user_id', 'left')
                ->where($where)
                ->where('order.status', 'neq', 'unpay')
                ->order($sort, $order)
                ->paginate($limit)
                ->each(function ($item) {
                    $item->avatar = $item->avatar ? cdnurl($item->avatar, true) : letter_avatar($item->nickname);
                });

            $totalMoney = $this->model->alias('order')
                ->join('user user', 'user.id = order.user_id', 'left')
                ->where($where)
                ->where('order.status', 'neq', 'unpay')
                ->sum('rmb_amount');

            // 清空对应角标
            $filter = json_decode(input('filter'), true);
            if ($filter && !$list->isEmpty()) {
                \app\admin\model\trade\Order::where('backend_read', 0)->where('status', $list[0]->status)->update(['backend_read' => 1]);
            }

            $result = array("total" => $list->total(), "rows" => $list->items(), 'totalmoney' => $totalMoney);

            return json($result);
        }

        // 渲染角标数字
        $sidebar = [
            'all' => \app\admin\model\trade\Order::where('backend_read', 0)->where('status', 'neq', 'unpay')->count(),
//            'unpay' => \app\admin\model\trade\Order::where('backend_read', 0)->where('status', 'unpay')->count(),
            'unused' => \app\admin\model\trade\Order::where('backend_read', 0)->where('status', 'unused')->count(),
            'used' => \app\admin\model\trade\Order::where('backend_read', 0)->where('status', 'used')->count(),
            'refund' => \app\admin\model\trade\Order::where('backend_read', 0)->where('status', 'refund')->count(),
        ];
        $this->assignconfig('sidebar_number', $sidebar);
        $this->assign('sidebar_number', $sidebar);

        return $this->view->fetch();
    }

    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row->userinfo = [
            'nickname' => '',
            'mobile' => '',
            'avatar' => '/assets/img/avatar.png'
        ];
        $user = User::field('nickname,mobile,avatar')->where('id', $row->user_id)->find();
        if ($user) {
            $user->avatar = $user->avatar ? cdnurl($user->avatar, true) : letter_avatar($user->nickname);
            $row->userinfo = $user;
        }

        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
