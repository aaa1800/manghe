<?php

namespace app\admin\controller\trade;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 中奖记录管理
 *
 * @icon fa fa-circle-o
 */
class Prizerecord extends Backend
{

    /**
     * Prizerecord模型对象
     * @var \app\admin\model\trade\Prizerecord
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\trade\Prizerecord;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model->alias('prize')
                ->field('prize.id,prize.out_trade_no,prize.goods_name,prize.goods_image,prize.goods_coin_price,prize.goods_rmb_price,prize.status,prize.exchange_time,prize.delivery_time,prize.create_time,prize.update_time received_time')
                ->field('user.nickname,user.avatar,user.mobile')
                ->field('order.coin_price box_coin_price,order.rmb_price box_rmb_price')
                ->join('user user', 'user.id = prize.user_id')
                ->join('order order', 'order.id = prize.order_id')
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit)
                ->each(function ($item) {
                    $item->avatar = $item->avatar ? cdnurl($item->avatar, true) : letter_avatar($item->nickname);
                });

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function edit($ids = null)
    {
        $row = $this->model->alias('prize')
            ->field('prize.id,prize.out_trade_no,prize.goods_name,prize.goods_image,prize.goods_coin_price,prize.goods_rmb_price,prize.status,prize.exchange_time,prize.delivery_time,prize.create_time')
            ->field('user.nickname,user.avatar,user.mobile')
            ->join('user user', 'user.id = prize.user_id')
            ->where('prize.id', $ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (\Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
