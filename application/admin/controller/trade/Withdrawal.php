<?php

namespace app\admin\controller\trade;

use app\admin\model\User;
use app\api\model\MoneyRecord;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 提现记录管理
 *
 * @icon fa fa-circle-o
 */
class Withdrawal extends Backend
{

    /**
     * Withdrawal模型对象
     * @var \app\admin\model\trade\Withdrawal
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\trade\Withdrawal;
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model->alias('record')
                ->field('record.*')
                ->field('user.nickname,user.avatar')
                ->join('user user', 'user.id = record.user_id')
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit)
                ->each(function ($item) {
                    $item->avatar = $item->avatar ? cdnurl($item->avatar, true) : letter_avatar($item->nickname);
                });

            $totalMoney = $this->model->alias('record')->join('user user', 'user.id = record.user_id')->where($where)->sum('amount');

            // 清空对应角标
            $filter = json_decode(input('filter'), true);
            if ($filter && !$list->isEmpty()) {
                \app\admin\model\trade\Withdrawal::where('backend_read', 0)->where('status', $list[0]->status)->update(['backend_read' => 1]);
            }

            $result = array("total" => $list->total(), "rows" => $list->items(), 'totalmoney' => $totalMoney);

            return json($result);
        }

        // 渲染角标数字
        $sidebar = [
            'all' => \app\admin\model\trade\Withdrawal::where('backend_read', 0)->count(),
            'review' => \app\admin\model\trade\Withdrawal::where('backend_read', 0)->where('status', 'review')->count(),
            'success' => \app\admin\model\trade\Withdrawal::where('backend_read', 0)->where('status', 'success')->count(),
            'reject' => \app\admin\model\trade\Withdrawal::where('backend_read', 0)->where('status', 'reject')->count(),
        ];
        $this->assignconfig('sidebar_number', $sidebar);
        $this->assign('sidebar_number', $sidebar);
        return $this->view->fetch();
    }

    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ('review' != $row->status) {
                    $this->error('进制修改已审核的申请');
                }

                // 驳回申请，则驳回原因不能为空
                if ('reject' == $params['status'] && empty($params['reason'])) {
                    $this->error('驳回原因不能为空');
                }

                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }

                    // 驳回申请，则增加用户余额
                    if ('reject' == $params['status']) {
                        $user = User::where('id', $row->user_id)->find();
                        $money_before = $user->retail_balance;
                       
                        $row->drawal_type? $user->setInc('retail_balance', $row->amount) : $user->setInc('money', $row->amount);

                        // // 创建余额记录
                        // MoneyRecord::create([
                        //     'user_id' => $row->user_id,
                        //     'before' => $money_before,
                        //     'after' => $user->money,
                        //     'money' => $row->amount,
                        //     'type' => 'withdrawal_fail', // 变更类型:box_exchange=盲盒回收,refund=库存不足，支付返回,withdrawal=提现,to_coin=转到钱包，withdrawal_fail=提现失败
                        // ]);
                    }

                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (\Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function multi($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            if ($this->request->has('params')) {
                parse_str($this->request->post("params"), $values);
                $values = $this->auth->isSuperAdmin() ? $values : array_intersect_key($values, array_flip(is_array($this->multiFields) ? $this->multiFields : explode(',', $this->multiFields)));
                if ($values) {
                    $adminIds = $this->getDataLimitAdminIds();
                    if (is_array($adminIds)) {
                        $this->model->where($this->dataLimitField, 'in', $adminIds);
                    }
                    $count = 0;
                    Db::startTrans();
                    try {
                        $list = $this->model->where($this->model->getPk(), 'in', $ids)->select();
                        foreach ($list as $index => $item) {

                            if ('review' != $item->status) {
                                throw new \Exception('不能修改已审核的申请');
                            }

                            // 驳回申请，则增加用户余额
                            if (isset($values['status']) && 'reject' == $values['status']) {
                                $user = User::where('id', $item->user_id)->find();
                                $money_before = $user->money;
                                $user->setInc('money', $item->amount);

                                // 创建余额记录
                                MoneyRecord::create([
                                    'user_id' => $item->user_id,
                                    'before' => $money_before,
                                    'after' => $user->money,
                                    'money' => $item->amount,
                                    'type' => 'withdrawal_fail', // 变更类型:box_exchange=盲盒回收,refund=库存不足，支付返回,withdrawal=提现,to_coin=转到钱包，withdrawal_fail=提现失败
                                ]);
                            }

                            $count += $item->allowField(true)->isUpdate(true)->save($values);
                        }
                        Db::commit();
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (\Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                    if ($count) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                } else {
                    $this->error(__('You have no permission'));
                }
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

}
