<?php

namespace app\admin\controller\goods;

use app\common\controller\Backend;

/**
 * 商品管理
 *
 * @icon fa fa-circle-o
 */
class Goods extends Backend
{
    protected $modelValidate = true;

    /**
     * Goods模型对象
     * @var \app\admin\model\goods\Goods
     */
    protected $model = null;

    protected $indexField = 'delete_time';
    protected $indexFieldExcept = true;
    protected $noNeedRight = ['selectpage'];


    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\goods\Goods;
        $this->view->assign("tagList", $this->model->getTagList());
        $this->view->assign("statusList", $this->model->getStatusList());
    }

    public function import()
    {
        parent::import();
    }

    public function selectpage()
    {
        return parent::selectpage();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $where_status = ['status'=>'online'];
            $filter = json_decode(input('filter'),true);
            if ($filter) {
                $where_status = [];
            }

            $list = $this->model
                ->field($this->indexField, $this->indexFieldExcept)
                ->where($where)
                ->where($where_status)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());
//print_r($result);die;
            return json($result);
        }
        // tab默认选项
        $this->assignconfig('tab_default','#tab-status-online');
        return $this->view->fetch();
    }

}
