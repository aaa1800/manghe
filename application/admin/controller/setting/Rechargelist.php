<?php

namespace app\admin\controller\setting;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 充值选择列管理
 *
 * @icon fa fa-circle-o
 */
class Rechargelist extends Backend
{

    protected $modelValidate = true;

    /**
     * Rechargelist模型对象
     * @var \app\admin\model\setting\Rechargelist
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\setting\Rechargelist;

        $one_rmb_to_coin = \app\admin\model\setting\Setting::value('one_rmb_to_coin_num');
        $this->assignconfig('one_rmb_to_coin', round($one_rmb_to_coin, 2));
        $this->assign('one_rmb_to_coin', round($one_rmb_to_coin, 2));
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

}
