<?php

namespace app\admin\validate\goods;

use think\Validate;

class Goods extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'goods_name' => 'require',
        'stock' => 'egt:0',
        'coin_price' => 'integer',
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];

    protected $field = [
        'goods_name' => '商品名称',
        'stock' => '库存',
        'coin_price' => '金币价格',
    ];
    
}
