<?php

namespace app\admin\validate\goods;

use think\Validate;

class Pricerange extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'range' => 'require|max:100'
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证字段描述
     */
    protected $field = [
        'range' => '价格区间'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
