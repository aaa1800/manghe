<?php

namespace app\admin\validate\setting;

use think\Validate;

class Rechargelist extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'coin' => 'require|integer|gt:0',
        'rmb' => 'require|number|gt:0',
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证字段描述
     */
    protected $field = [
        'coin' => '金币',
        'rmb' => 'RMB'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
