<?php

namespace app\admin\validate\setting;

use think\Validate;

class Version extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'platform' => 'require|in:h5,android,ios,applets',
        'version' => 'require|max:30',
        'content' => 'require',
        'download_file' => 'require',
        'enforce' => 'require|in:1,0',
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证字段描述
     */
    protected $field = [
        'platform' => '平台',
        'version' => '版本号',
        'content' => '更新内容',
        'download_file' => '下载地址',
        'enforce' => '强制更新',
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
