<?php

namespace app\admin\validate\setting;

use think\Validate;

class Banner extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'image' => 'require',
        'type' => 'require|in:box,link,word',
        'status' => 'require|in:normal,hidden',
    ];
    /**
     * 提示消息
     */
    protected $message = [
        'type.in' => '类别只能是：链接、盲盒、文本',
        'status.in' => '状态只能是：正常，隐藏',
    ];
    /**
     * 验证字段描述
     */
    protected $field = [
        'image' => '图片',
        'type' => '类别',
        'status' => '状态'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
