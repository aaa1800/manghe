<?php

namespace app\admin\validate\box;

use think\Validate;

class Category extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'name' => 'unique:category'
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    protected $field = [
        'name' => '分类名称'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => ['weigh']
    ];
    
}
