<?php

namespace app\admin\validate\box;

use think\Validate;

class Detail extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'rate' => 'float|egt:0'
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证字段描述
     */
    protected $field = [
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
