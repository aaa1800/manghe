<?php

namespace app\admin\validate\box;

use think\Validate;

class Box extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'coin_price' => 'number|gt:0'
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证字段描述
     */
    protected $field = [
        'coin_price' => '金币价格'
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
