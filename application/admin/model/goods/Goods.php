<?php

namespace app\admin\model\goods;

use app\admin\model\box\Detail;
use app\admin\model\setting\Setting;
use think\Model;
use traits\model\SoftDelete;


class Goods extends Model
{
    use SoftDelete;

    // 表名
    protected $name = 'goods';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';

    // 追加属性
    protected $append = [
        'tag_text',
        'status_text',
        'rmb_price',
        'create_time_text',
        'update_time_text',
        'delete_time_text'
    ];

    protected static function init()
    {
        Goods::afterDelete(function ($goods) {
            // 删除时同步删除盲盒中的商品
            $boxDetails = Detail::field('id')->where('goods_id', $goods->id)->select();
            foreach ($boxDetails as $item) {
                $item->delete();
            }
            return true;
        });

        Goods::afterUpdate(function ($goods) {
            if (!empty($goods->status) && 'offline' == $goods->status) {
                // 下架商品同步删除盲盒中的商品
                $boxDetails = Detail::field('id')->where('goods_id', $goods->id)->select();
                foreach ($boxDetails as $item) {
                    $item->delete();
                }
            }
            return true;
        });
    }

    public function getTagList()
    {
        return ['normal' => __('Tag normal'), 'rare' => __('Tag rare'), 'supreme' => __('Tag supreme'), 'legend' => __('Tag legend')];
    }

    public function getStatusList()
    {
        return ['online' => __('Status online'), 'offline' => __('Status offline')];
    }

    public function getTagTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['tag']) ? $data['tag'] : '');
        $list = $this->getTagList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getCreateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['create_time']) ? $data['create_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getUpdateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['update_time']) ? $data['update_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getDeleteTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['delete_time']) ? $data['delete_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    public function getRmbPriceAttr($value, $data)
    {
        if (empty($data['coin_price'])) {
            return 0;
        }

        return round(Setting::getRmbFromCoin($data['coin_price'] ?: 0), 2);
    }

    protected function setCreateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setUpdateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setDeleteTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
