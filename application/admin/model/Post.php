<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class Post extends Model
{

    use SoftDelete;

    protected $name = 'post';

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';

    public static function getPostList()
    {
        return self::column('post_name','post_code');
    }
}
