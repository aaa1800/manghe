<?php

namespace app\admin\model\setting;

use think\Model;

class Text extends Model
{
    // 表名
    protected $name = 'text';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = 'update_time';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'text_short',
    ];


    public function getTypeList()
    {
        return ['rich_text' => __('Rich_text'), 'text' => __('Text')];
    }

    public function getTextShortAttr($value, $data)
    {
        if (!empty($data['text'])) {
            $text = mb_substr(strip_tags($data['text']), 0, 50);
            return mb_strlen($text) >= 50 ? $text . '...' : $text;
        }
        return '';
    }

    public function getUpdateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['update_time']) ? $data['update_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setUpdateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
