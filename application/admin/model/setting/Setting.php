<?php

namespace app\admin\model\setting;

use think\Model;


class Setting extends Model
{

    // 表名
    protected $name = 'setting';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = 'update_time';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'update_time_text'
    ];

    public function getUpdateTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['update_time']) ? $data['update_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setUpdateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    /**
     * RMB兑换金币
     * @param float $rmb
     * @return false|float
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/09 15:12
     */
    public static function getCoinFromRmb(float $rmb)
    {
        if (empty($rmb)) return 0;
        $rate = self::where('id', 1)->value('one_rmb_to_coin_num');
        return round(floatval($rmb) * $rate, 5);
    }

    /**
     * 金币兑换RMB
     * @param float $coin
     * @return false|float
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/09 15:13
     */
    public static function getRmbFromCoin(float $coin)
    {
        if (empty($coin)) return 0;
        $rate = self::where('id', 1)->value('one_rmb_to_coin_num');
        return round(floatval($coin) / $rate, 5);
    }


}
