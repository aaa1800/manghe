<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\User;

use think\Env;
use think\Hook;
use think\Db;
use app\api\model\Carpass as Carpassmodel;
/**
 * 卡密充值
 */
class Carpass extends Api
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    
public function deposit()
    {
         $user =   $this->auth->id;
         $password = input('password');
      //  $password = $this->request->post("password");
       // $user = input('user');
        
       // print_r($password);die;
        $carpass = db('carpass')->where('password', $password)->where('status', '0')->find();
        
       $time = date("Y-m-d H:i:s",time());
      if($carpass != null){
          $price = db('carpass')->where('password', $password)->value('price');
            db('carpass')->where('password', $password)->update(['status'=>1]);
            db('carpass')->where('password', $password)->update(['etime'=>$time]);
            db('carpass')->where('password', $password)->update(['user'=>$user]);
            $money = db('user')->where('id', $user)->value('coin');
            db('user')->where('id', $user)->update(['coin'=>$money+$price]);
            $this->success('卡密兑换成功！');
          
      }else{
          $this->error('卡密已使用或者输入卡密有误');
      }

       
    }

    
    public function check()
    {
       $user =   $this->auth->id;
         
         $carpass = db('carpass')->where('user', $user)->where('status', '1')->select();
         
        
        if($carpass){
            $this->success('查询成功', $carpass);
        } else{
            $this->error('暂无卡密充值记录');
        }
        
      
    } 
}
