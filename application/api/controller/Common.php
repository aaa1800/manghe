<?php

namespace app\api\controller;

use app\api\model\Text;
use app\common\controller\Api;
use app\common\exception\UploadException;
use app\common\library\Upload;
use fuyelk\yapi\Yapi;
use think\Config;

/**
 * 公共接口
 */
class Common extends Api
{
    protected $noNeedLogin = ['agreement', 'updateApiDoc', 'gitpull'];
    protected $noNeedRight = '*';

    /**
     * 上传文件
     * @ApiMethod (POST)
     * @param \think\File $file 文件流
     */
    public function upload()
    {
        Config::set('default_return_type', 'json');
        //必须设定cdnurl为空,否则cdnurl函数计算错误
        Config::set('upload.cdnurl', '');

        // 切片
        $chunkid = $this->request->post("chunkid");
        if ($chunkid) {
            if (!Config::get('upload.chunking')) {
                $this->error(__('Chunk file disabled'));
            }
            $action = $this->request->post("action");
            $chunkindex = $this->request->post("chunkindex/d");
            $chunkcount = $this->request->post("chunkcount/d");
            $filename = $this->request->post("filename");
            $method = $this->request->method(true);
            if ($action == 'merge') {
                $attachment = null;
                //合并分片文件
                try {
                    $upload = new Upload();
                    $attachment = $upload->merge($chunkid, $chunkcount, $filename);
                } catch (UploadException $e) {
                    $this->error($e->getMessage());
                }
                $this->success(__('Uploaded successful'), ['url' => $attachment->url, 'fullurl' => cdnurl($attachment->url, true)]);
            }

            if ($method == 'clean') {
                //删除冗余的分片文件
                try {
                    $upload = new Upload();
                    $upload->clean($chunkid);
                } catch (UploadException $e) {
                    $this->error($e->getMessage());
                }
                $this->success();
            }

            //上传分片文件
            //默认普通上传文件
            $file = $this->request->file('file');
            try {
                $upload = new Upload($file);
                $upload->chunk($chunkid, $chunkindex, $chunkcount);
            } catch (UploadException $e) {
                $this->error($e->getMessage());
            }
            $this->success();

        }

        $attachment = null;
        //默认普通上传文件
        $file = $this->request->file('file');
        try {
            $upload = new Upload($file);
            $attachment = $upload->upload();
        } catch (UploadException $e) {
            $this->error($e->getMessage());
        }

        $this->success('上传成功', ['url' => $attachment->url, 'fullurl' => cdnurl($attachment->url, true)]);
    }

    /**
     * 协议、规则
     * @param string $name 协议名称
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/05 20:48
     */
    public function agreement()
    {
        $name = input('name');
        if (empty($name)) {
            $this->error('协议名称不能为空');
        }

        $content = Text::getText($name);
        $this->success('查询成功', ['content' => $content]);
    }

    /**
     * 更新接口文档
     */
    public function updateApiDoc()
    {
        $fileName = '/doc/rVs4ULys17.html';
        $yapi = new Yapi('h5box@h5box.com', 'eeAW3ECjRB551vwO', 'http://api.fuyelk.com:3000');
        $res = $yapi->export(227, 'html', ROOT_PATH . 'public' . $fileName);
        if (!$res) {
            $this->error($yapi->getError());
        }
        $this->success('文档更新成功', ['url' => $this->request->domain() . $fileName]);
    }

    /**
     * 码云git钩子
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/08 11:15
     */
    public function gitpull()
    {
        $token = $this->request->server('HTTP_X_GITEE_TOKEN');
        if ('LUY})!1@#M,StanM\DR\}AKz*@dMZ[5*' != $token) {
            $this->error('验权失败', [], '401');
        }
        try {
            $path = ROOT_PATH;
            exec("cd {$path} && git pull 2>&1");
            // 注意：windows服务器可能需要将.ssh下的文件拷贝到C:\Windows\System32\config\systemprofile\.ssh\下
        } catch (\Exception $e) {
            dta('码云代码拉取失败');
            $this->error('代码拉取失败', ['error' => $e->getMessage()]);
        }
        $this->success('成功');
    }
}
