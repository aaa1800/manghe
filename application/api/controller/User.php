<?php

namespace app\api\controller;

use app\api\model\DeliveryTrade;
use app\api\model\Detail;
use app\api\model\Goods;
use app\api\model\MoneyRecord;
use app\api\model\CoinRecord;
use app\api\model\Delivery;
use app\api\model\Order;
use app\api\model\Zz;
use app\api\model\Prizerecord;
use app\api\model\SearchHistory;
use app\api\model\Setting;
use app\api\model\Star;
use app\api\model\Text;
use app\api\model\UserAddress;
use app\api\model\Version;
use app\api\model\Withdrawal;
use app\common\controller\Api;
use app\common\library\Sms;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
use fast\Random;
use think\Db;
use think\Exception;
use think\Validate;

/**
 * 会员接口
 */
class User extends Api
{
    protected $noNeedLogin = ['login', 'mobilelogin', 'register', 'resetpwd', 'changeemail', 'changemobile', 'third','userCapital','lunbobox'];
    protected $noNeedRight = '*';

    protected $lockUserId = ['applyDelivery', 'exchange', 'moneyToCoin', 'withdrawal'];

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 手机验证码登录
     *
     * @param string $mobile 手机号
     * @param string $captcha 验证码
     */
    public function mobilelogin()
    {
        $mobile = input('mobile');
        $captcha = input('captcha');
        $invite_code = input('sharecode', '');
        $is_channel = input('is_channel', '');  
        //print_r($invite_code);//是否特定渠道进来
        $is_notice = 0; //是否弹窗
        if (!$mobile || !$captcha) {
            $this->error('参数错误');
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error('手机号格式不正确');
        }
        if (!Sms::check($mobile, $captcha, 'login') && !Sms::check($mobile, $captcha, 'register')) {
            // $this->error('验证码不正确');
        }
        $user = \app\common\model\User::getByMobile($mobile);
        if ($user) {
            if ($user->status != 'normal') {
                $this->error('账号被锁定');
            }
            //如果已经有账号则直接登录
            $ret = $this->auth->direct($user->id);
        } else {
            $ret = $this->auth->register($mobile, Random::alnum(), '', $mobile, ['invite_code' => $invite_code]);

            if($ret && $is_channel == '49ba59abbe56e057'){  //赠送10金币
                \app\common\model\User::where(['id'=>$this->auth->id])->setInc("coin", 10);
                $is_notice = 1;
            }
        }
        if ($ret) {
            Sms::flush($mobile, 'login');
            $data = $this->auth->getUserinfo();
            $data['money'] = $data['money'] ? floatval($data['money']) : 0;
            $data['coin'] = $data['coin'] ? floatval($data['coin']) : 0;
            $data['avatar'] = $data['avatar'] ? cdnurl($data['avatar'], true) : '';
            $data['is_notice'] = $is_notice;
            unset($data['id']);
            unset($data['user_id']);
            unset($data['createtime']);
            unset($data['expiretime']);
            unset($data['expires_in']);
              $registerurl = db('setting')->where('id', 1)->find();   
            
            
            if($registerurl['registerurl'] == NULL){
                $data['un'] = 1;
                 $this->success('注册成功',$data);
            }else{
                $data['registerurl'] = $registerurl['registerurl'];
                $data['un'] = 0;
                 $this->success('注册成功',$data);
            }
            
            
            $this->success('登录成功', $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 账号密码登录
     */
    public function login()
    {
        $account = input('mobile');
        $password = input('password');
        if (!$account || !$password) {
            $this->error('账号和密码不能为空');
        }
        $ret = $this->auth->login($account, $password);
        if ($ret) {
            $data = $this->auth->getUserinfo();
            $data['money'] = $data['money'] ? floatval($data['money']) : 0;
            $data['avatar'] = $data['avatar'] ? cdnurl($data['avatar'], true) : '';
            unset($data['id']);
            unset($data['user_id']);
            unset($data['createtime']);
            unset($data['expiretime']);
            unset($data['expires_in']);
            $this->success('登录成功', $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注册会员
     */
    public function register()
    {
        $mobile = input('mobile');
        $password = input('password');
        $captcha = input('captcha');
        $invite_code = input('sharecode', '');
        if (!$mobile || !$password) {
            $this->error('参数错误');
        }
        if ($mobile && !Validate::regex($mobile, "^1\d{10}$")) {
            $this->error('手机号格式不正确');
        }
        $ret = Sms::check($mobile, $captcha, 'register');
        if (!$ret) {
            $this->error('验证码不正确');
        }
        $ret = $this->auth->register($mobile, $password, '', $mobile, ['invite_code' => $invite_code]);
        
        
        if ($ret) {
            
             $registerurl = db('setting')->where('id', 1)->find();   
     
            
            Sms::flush($mobile, 'register');
            $data = $this->auth->getUserinfo();
            $data['money'] = $data['money'] ? floatval($data['money']) : 0;
            $data['avatar'] = $data['avatar'] ? cdnurl($data['avatar'], true) : '';
            unset($data['id']);
            unset($data['user_id']);
            unset($data['createtime']);
            unset($data['expiretime']);
            unset($data['expires_in']);
            
           
            if($registerurl['registerurl'] == NULL){
                $data['un'] = 1;
                 $this->success('注册成功',$data);
            }else{
                $data['registerurl'] = $registerurl['registerurl'];
                $data['un'] = 0;
                 $this->success('注册成功',$data);
            }
           
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 重置密码（忘记密码）
     */
    public function resetpwd()
    {
        $mobile = input("mobile");
        $newpassword = input("newpassword");
        $captcha = input("captcha");
        if (!$newpassword || !$captcha) {
            $this->error('参数错误');
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error('手机号格式不正确');
        }
        if (!Validate::checkRule($newpassword, "min:8|max:12")) {
            $this->error('请设置8-12位密码');
        }
        $user = \app\common\model\User::getByMobile($mobile);
        if (!$user) {
            $this->error('用户不存在');
        }
        $ret = Sms::check($mobile, $captcha, 'resetpwd');
        if (!$ret) {
            $this->error('验证码不正确');
        }
        Sms::flush($mobile, 'resetpwd');
        //模拟一次登录
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, '', true);
        if ($ret) {
            $this->success('重置密码成功');
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 修改密码
     */
    public function changepwd()
    {
        $oldpassword = input("oldpassword");
        $newpassword = input("newpassword");
        if (!$newpassword || !$oldpassword) {
            $this->error('参数错误');
        }
        if (!Validate::checkRule($newpassword, "min:8|max:12")) {
            $this->error('请设置8-12位密码');
        }
        $ret = $this->auth->changepwd($newpassword, $oldpassword, false);
        if ($ret) {
            $this->success('修改密码成功');
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        $this->auth->logout();
        $this->success('退出成功');
    }

    /**
     * 修改手机号
     *
     * @param string $mobile 手机号
     * @param string $captcha 验证码
     */
    public function changemobile()
    {
        $user = $this->auth->getUser();
        $mobile = input('mobile');
        $captcha = input('captcha');
        if (!$mobile || !$captcha) {
            $this->error('参数错误');
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error('手机号格式不正确');
        }
        if (\app\common\model\User::where('mobile', $mobile)->where('id', '<>', $user->id)->find()) {
            $this->error('手机号已存在');
        }
        $result = Sms::check($mobile, $captcha, 'changemobile');
        if (!$result) {
            $this->error('验证码不正确');
        }
        $verification = $user->verification;
        $verification->mobile = 1;
        $user->verification = $verification;
        $user->mobile = $mobile;
        $user->save();

        Sms::flush($mobile, 'changemobile');
        $this->success('修改成功');
    }

    /**
     * 绑定手机号
     *
     * @param string $mobile 手机号
     * @param string $captcha 验证码
     */
    public function bindMobile()
    {
        $user = $this->auth->getUser();
        $mobile = input('mobile');
        $captcha = input('captcha');
        if (!$mobile || !$captcha) {
            $this->error('参数错误');
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error('手机号格式不正确');
        }
        if (\app\common\model\User::where('mobile', $mobile)->where('id', '<>', $user->id)->find()) {
            $this->error('手机号已存在');
        }
        $result = Sms::check($mobile, $captcha, 'bindmobile');
        if (!$result) {
            $this->error('验证码不正确');
        }
        $verification = $user->verification;
        $verification->mobile = 1;
        $user->verification = $verification;
        $user->mobile = $mobile;
        $user->save();

        Sms::flush($mobile, 'bindmobile');
        $this->success('绑定成功');
    }
    
    
    
    // 用户金币 和 资产
    public function userCapital(){
        
        if($this->auth->id){
          $box = db('prize_record')->where(['user_id' => $this->auth->id,'status' => 'bag'])->count();    
          $ret = ['coin' => $this->auth->coin,'box' => $box];              
        }else{
          $ret = ['coin' => 0,'box' => 0];   
        }
        $this->success('查询成功', $ret);
    }
    

    /**
     * 用户信息
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/08 15:54
     */
    public function userinfo()
    {
        
        $openurl = db('setting')->where('id', 1)->find();   
       // print_r($openurl['openurl'] . '?sharecode=' . $this->auth->sharecode);die;
      $qrcode = new QrCode($openurl['openurl'] . '?sharecode=' . $this->auth->sharecode);
     //  $qrcode = new QrCode($this->request->domain() . '/h5/#/?sharecode=' . $this->auth->sharecode);
        $avatar = db('user')->where('id', $this->auth->id)->value('avatar');
        if ($avatar) {
            $qrcode->setLogoPath(cdnurl($avatar, true));
            $qrcode->setLogoSize(80);
        }
        $qrcode->setErrorCorrectionLevel(ErrorCorrectionLevel::QUARTILE());
        $qrcode_image = $qrcode->writeDataUri();
        $ret = [
            'nickname' => $this->auth->nickname,
            'mobile' => $this->auth->mobile,
             'settled_item' => $this->auth->retail_balance,
            'avatar' => $this->auth->avatar ? cdnurl($this->auth->avatar, true) : '',
            'money' => floatval($this->auth->money),
            'coin' => $this->auth->coin,
            'sharecode' => $this->auth->sharecode,
            'qr_code' => $qrcode_image
        ];

        $this->success('查询成功', $ret);
    }
   

    /**
     * 我的盒柜
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/12 00:29
     */
    public function myBox()
    {
        $status = input('status/d');
        $pagesize = input('pagesize/d', 10);
        $page = input('page/d', 1);
        $statusList = [1 => 'bag', 2 => 'exchange'];

        if (!isset($statusList[$status])) {
            $this->error('状态有误');
        }
        $status = $statusList[$status];

        $order = 'prize.id desc';
        if ('exchange' == $status) {
            $order = 'exchange_time desc';
        }
        
       
       
        
      
        $list = Prizerecord::alias('prize')
            ->field('prize.id record_id,prize.goods_name,prize.goods_image,prize.create_time,prize.exchange_time,prize.goods_coin_price,prize.goods_rmb_price')
            ->field('order.coin_price box_coin_price,order.rmb_price box_rmb_price,order.pay_method')
            ->join('order order', 'order.id = prize.order_id')
            ->where('prize.user_id', $this->auth->id)
            ->where('prize.status', $status) // 奖品状态:bag=盒柜,exchange=已回收,delivery=申请发货,received=已收货
            ->order($order)
            ->paginate($pagesize, false, ['page' => $page])
            ->each(function ($item) use ($status) {
                 $registerurl = db('setting')->where('id', 1)->find(); 
                 $recovery_discount = floatval($registerurl['recovery_discount']);
                $item->goods_image = $item->goods_image ? cdnurl($item->goods_image, true) : '';

                $item->box_coin_price = intval($item->box_coin_price);
                $item->box_rmb_price = floatval($item->box_rmb_price);
                $item->goods_coin_price = intval($item->goods_coin_price);
                $item->goods_rmb_price = floatval($item->goods_rmb_price);
                $item->goods_hui_price = floor($item->goods_coin_price * round($recovery_discount / 100, 2));

                if ('exchange' == $status) {
                    $item->time = date('Y-m-d H:i:s', $item->exchange_time);
                } else {
                    $item->time = date('Y-m-d H:i:s', $item->create_time);
                }
                $item->hidden(['create_time', 'exchange_time']);
            });

        $this->success('查询成功', $list);
    }

    /**
     * 开箱记录
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function openRecord()
    {
        $pagesize = input('pagesize/d', 10);
        $page = input('page/d', 1);

        $list = Order::field('box_name,image,num,coin_amount,pay_time')
            ->where('status', 'used') // 状态:unpay=待支付,unused=待抽奖,used=已使用
            ->where('user_id', $this->auth->id)
            ->paginate($pagesize, false, ['page' => $page])
            ->each(function ($item) {
                $item->box_name = $item->box_name . ' x' . $item->num;
                $item->image = $item->image ? cdnurl($item->image, true) : '';
                $item->time = date('Y-m-d H:i:s', $item->pay_time);
                $item->hidden(['pay_time', 'num']);
            });
        $this->success('查询成功', $list);
    }

    /**
     * 我的订单列表
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function myOrderList()
    {
        $status = input('status/d');
        $pagesize = input('pagesize/d', 10);
        $page = input('page/d', 1);

        $statusList = [1 => 'undelivered', 2 => 'unreceived', 3 => 'finished'];
        $statusListText = ['undelivered' => '待发货', 'unreceived' => '已发货', 'finished' => '已完成'];
        if (!isset($statusList[$status])) {
            $this->error('状态有误');
        }
        $status = $statusList[$status];

        $list = Delivery::alias('delivery')
            ->field('delivery.id delivery_order_id,delivery.delivery_order_no,delivery.goods_name,delivery.goods_image,delivery.create_time,delivery.delivery_number,delivery.post_name')
            ->field('prize.goods_coin_price')
            ->join('prize_record prize', 'prize.id = delivery.prize_id')
            ->where('delivery.user_id', $this->auth->id)
            ->where('delivery.status', 'neq', 'unpay')
            ->where('delivery.status', $status)
            ->order('delivery.delivery_time desc,delivery.id desc')
            ->paginate($pagesize, false, ['page' => $page])
            ->each(function ($item) use ($status, $statusListText) {
                $item->status = $statusListText[$status];
                $item->goods_image = $item->goods_image ? cdnurl($item->goods_image, true) : '';
                $item->time = date('Y-m-d H:i:s', $item->create_time);
                $item->hidden(['create_time']);
                $item->delivery_number = $item->delivery_number ?: '';
                $item->post_name = $item->post_name ?: '';
            });

        $this->success('查询成功', $list);
    }

    /**
     * 发货订单详情
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function deliveryOrderDetail()
    {
        $delivery_order_id = input('delivery_order_id');
        if (empty($delivery_order_id)) {
            $this->error('未选择订单');
        }
        $order = Delivery::alias('delivery')
            ->field('delivery.id delivery_order_id,delivery.delivery_order_no,delivery.username,delivery.mobile,
            delivery.province,delivery.city,delivery.area,delivery.detail,delivery.address,
            delivery.goods_name,delivery.goods_image,delivery.create_time delivery_apply_time,
            delivery.status,delivery.delivery_time,delivery.delivery_number,delivery.post_code,delivery.post_name,delivery.receive_time')
            ->field('order.coin_price,order.rmb_price,order.pay_method,order.pay_time box_open_time')
            ->join('order order', 'order.id = delivery.order_id', 'left')
            ->where('delivery.status', 'neq', 'unpay')
            ->where('delivery.id', $delivery_order_id)
            ->where('delivery.user_id', $this->auth->id)
            ->find();

        if (empty($order)) {
            $this->error('订单有误');
        }

        $order->delivery_apply_time = date('Y-m-d H:i:s', $order->delivery_apply_time);
        $order->box_open_time = date('Y-m-d H:i:s', $order->box_open_time);
        $order->delivery_time = $order->delivery_time ? date('Y-m-d H:i:s', $order->delivery_time) : '';
        $order->receive_time = $order->receive_time ? date('Y-m-d H:i:s', $order->receive_time) : '';
        $order->goods_image = $order->goods_image ? cdnurl($order->goods_image, true) : '';

        if ('coin' == $order->pay_method) {
            $order->pay_amount = $order->coin_price . '金币';
            $order->pay_method = '金币';
        } else {
            $order->pay_amount = $order->rmb_price . '元';

            if ('wechat' == $order->pay_method) {
                $order->pay_method = '金币';
            }
            if ('alipay' == $order->pay_method) {
                $order->pay_method = '支付宝';
            }
        }
        unset($order->coin_price);
        unset($order->rmb_price);

        $order->notice = Text::gettext('notice_to_buyers');

        $this->success('查询成功', $order);
    }

    /**
     * 确认收货
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function confirmReceipt()
    {
        $delivery_order_id = input('delivery_order_id');
        if (empty($delivery_order_id)) {
            $this->error('未选择订单');
        }

        $order = Delivery::field('id,status,prize_id')->where('id', $delivery_order_id)->where('status', 'unreceived')->find();
        if (empty($order)) {
            $this->error('订单有误');
        }

        $order->save(['status' => 'finished', 'receive_time' => time()]);

        // 更新奖品状态
        Prizerecord::where('id', $order->prize_id)->update(['status' => 'received']);

        $this->success('确认收货成功');
    }

    /**
     * 我的金币
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function myCoin()
    {
        $pagesize = input('pagesize/d', 10);
        $page = input('page/d', 1);
        $type = input('type');

        $type = trim($type);
        $whereType = [];
        if (!empty($type)) {
            if (!in_array($type, ['in', 'out'])) {
                $this->error('变动类型只能是：in或out');
            }
            $cond = 'in' == $type ? 'gt' : 'lt';
            $whereType = ['coin' => [$cond, 0]];
        }

        $typeList = ['pay_box' => '支付盲盒', 'recharge' => '充值', 'from_balance' => '余额转入', 'refund' => '库存不足，支付返回', 'pay_delivery' => '支付运费']; // 变更类型:pay_box=支付盲盒,recharge=充值,from_balance=余额转入,refund=库存不足，支付返回,pay_delivery=支付运费

        $list = CoinRecord::field('type,create_time,coin')
            ->where('user_id', $this->auth->id)
            ->where($whereType)
            ->order('id', 'desc')
            ->paginate($pagesize, false, ['page' => $page])
            ->each(function ($item) use ($typeList) {
                $item->create_time = date('Y-m-d H:i:s', $item->create_time);
                $item->type = $typeList[$item->type];
                $item->coin = $item->coin > 0 ? '+' . $item->coin : strval($item->coin);
            });

        $ret = [
            'balance' => $this->auth->coin,
            'record' => $list
        ];

        $this->success('查询成功', $ret);
    }

    /**
     * 我的余额
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function myBalance()
    {
        $pagesize = input('pagesize/d', 10);
        $page = input('page/d', 1);
        $type = input('type');

        $type = trim($type);
        $whereType = [];
        if (!empty($type)) {
            if (!in_array($type, ['in', 'out'])) {
                $this->error('变动类型只能是：in或out');
            }
            $cond = 'in' == $type ? 'gt' : 'lt';
            $whereType = ['money' => [$cond, 0]];
        }

        $typeList = [
            'box_exchange' => '盲盒回收',
            'no_stock' => '库存不足，支付返回',
            'withdrawal' => '佣金提现',
            'withdrawals' => '余额提现',
            'to_coin' => '转到钱包',
            'yezhifu' => '余额支付',
            'pay_delivery' => '支付运费',
        ];
            
            $list = MoneyRecord::field('type,create_time,money')
            ->where('user_id', $this->auth->id)
            ->where($whereType)
            ->order('id', 'desc')
            ->paginate($pagesize, false, ['page' => $page]);
         
            
        $list = MoneyRecord::field('type,create_time,money')
            ->where('user_id', $this->auth->id)
            ->where($whereType)
            ->order('id', 'desc')
            ->paginate($pagesize, false, ['page' => $page])
            ->each(function ($item) use ($typeList) {
                $item->create_time = date('Y-m-d H:i:s', $item->create_time);
                $item->type = $typeList[$item->type];
                $item->money = floatval($item->money);
                $item->money = $item->money > 0 ? '+' . $item->money : strval($item->money);
            });

        $ret = [
            'balance' => floatval($this->auth->money),
            'record' => $list
        ];

        $this->success('查询成功', $ret);
    }

    /**
     * 我的收藏
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function myStar()
    {
        $pagesize = input('pagesize/d', 10);
        $page = input('page/d', 1);

        $list = Star::alias('star')
            ->field('star.id star_id,star.create_time star_time')
            ->field('box.id box_id,box.box_name,box.coin_price')
            ->join('box box', 'box.id = star.box_id')
            ->where('star.user_id', $this->auth->id)
            ->order('star.id', 'desc')
            ->paginate($pagesize, false, ['page' => $page])
            ->each(function ($item) {
                // 查询盲盒中的第一个商品图片
                $goods_id = Detail::where('box_id', $item->box_id)->order('weigh', 'desc')->value('goods_id');
                $image = Goods::where('id', $goods_id)->value('image');
                $item->image = $image ? cdnurl($image, true) : '';
                $item->star_time = date('Y-m-d H:i:s', $item->star_time);
            });

        $this->success('查询成功', $list);
    }

    /**
     * 取消收藏
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/12 17:29
     */
    public function cancelStar()
    {
        $star_id = input('star_id/d');
        if (empty($star_id)) {
            $this->error('没有选择收藏');
        }

        Star::where('user_id', $this->auth->id)->where('id', $star_id)->delete();
        $this->success('已取消收藏');
    }

    /**
     * 我的收货地址
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function myAddress()
    {
        $is_default = input('is_default/d');
        $where_is_default = [];
        if (!empty($is_default)) {
            $where_is_default = ['is_default' => 1];
        }

        $list = UserAddress::field('id address_id,username,mobile,province,city,area,detail,is_default')
            ->where('user_id', $this->auth->id)
            ->where($where_is_default)
            ->select();

        foreach ($list as $item) {
            $item->is_default = !!$item->is_default;
            $item->full_address = $item->province . $item->city . $item->area . $item->detail;
        }

        $this->success('查询成功', $list);
    }

    /**
     * 添加收货地址
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function addAddress()
    {
        $params = input();
        $params['user_id'] = $this->auth->id;

        $rule = [
            'user_id' => 'require|integer',
            'username' => 'require|max:20',
            'mobile' => 'require|max:15',
            'province' => 'require|max:20',
            'city' => 'require|max:20',
            'area' => 'require|max:20',
            'detail' => 'require|max:50',
            'is_default' => 'integer|in:0,1',
        ];
        $field = [
            'username' => '收货人姓名',
            'mobile' => '收货人电话',
            'province' => '省份',
            'city' => '市级',
            'area' => '地区',
            'detail' => '详细地址',
            'is_default' => '是否默认',
        ];

        if (empty($params['is_default'])) {
            $params['is_default'] = 0;
        }

        $validate = new Validate($rule, [], $field);
        $result = $validate->check($params);
        if (!$result) {
            $this->error($validate->getError());
        }

        $address = new UserAddress();
        try {
            $res = $address->create($params, array_keys($rule));
            if ($params['is_default']) {
                UserAddress::where('user_id', $this->auth->id)->where('id', 'neq', $res->id)->update(['is_default' => 0]);
//            } else {
//                // 没有默认地址，则当前地址设为默认地址
//                if (!UserAddress::where('user_id', $this->auth->id)->where('is_default', 1)->value('id')) {
//                    $res->save(['is_default' => 1]);
//                }
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success('添加成功');
    }

    /**
     * 编辑收货地址
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function editAddress()
    {
        $params = input();
        $params['user_id'] = $this->auth->id;

        $rule = [
            'address_id' => 'require|integer',
            'user_id' => 'require|integer',
            'username' => 'require|max:20',
            'mobile' => 'require|max:15',
            'province' => 'require|max:20',
            'city' => 'require|max:20',
            'area' => 'require|max:20',
            'detail' => 'require|max:50',
            'is_default' => 'integer|in:0,1',
        ];
        $field = [
            'address_id' => '地址ID',
            'username' => '收货人姓名',
            'mobile' => '收货人电话',
            'province' => '省份',
            'city' => '市级',
            'area' => '地区',
            'detail' => '详细地址',
            'is_default' => '是否默认',
        ];

        if (empty($params['is_default'])) {
            $params['is_default'] = 0;
        }

        $validate = new Validate($rule, [], $field);
        $result = $validate->check($params);
        if (!$result) {
            $this->error($validate->getError());
        }

        $row = UserAddress::where('user_id', $this->auth->id)->where('id', $params['address_id'])->find();
        if (empty($row)) {
            $this->error('地址不存在');
        }

        unset($params['address_id']);
        unset($rule['address_id']);

        try {
            $row->allowfield(array_keys($rule))->save($params);
            if ($params['is_default']) {
                UserAddress::where('user_id', $this->auth->id)->where('id', 'neq', $row->id)->update(['is_default' => 0]);
//            } else {
//                // 没有默认地址，则当前地址设为默认地址
//                if (!UserAddress::where('user_id', $this->auth->id)->where('is_default', 1)->value('id')) {
//                    $row->save(['is_default' => 1]);
//                }
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success('修改成功');
    }

    /**
     * 删除收货地址
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function deleteAddress()
    {
        $id = input('address_id/d');
        if (empty($id)) {
            $this->error('未选择地址');
        }

        try {
            $row = UserAddress::where('user_id', $this->auth->id)->where('id', $id)->find();
        } catch (\Exception $e) {
            $this->error('删除失败');
        }
        if (empty($row)) {
            $this->error('地址不存在');
        }

        $row->delete();

        $this->success('删除成功');
    }

    /**
     * 获取设置信息
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function getSettingInfo()
    {
        $ret = [
            'avatar' => $this->auth->avatar ? cdnurl($this->auth->avatar, true) : letter_avatar($this->auth->nickname),
            'avatar_url' => $this->auth->avatar,
            'nickname' => $this->auth->nickname
        ];

        $this->success('查询成功', $ret);
    }

    /**
     * 检查新版本
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function checkVersion()
    {
        $currentVersion = input('current_version');
        $platform = input('platform');

        if (!in_array($platform, ['h5', 'android', 'ios', 'applets'])) {
            $this->error('选择的平台有误');
        }

        $lastVersion = Version::getLatest($currentVersion, $platform);

        $ret = [
            'has_new' => false,
            'new_version' => '',
            'enforce' => '',
            'download_url' => '',
            'content' => '',
        ];

        // 发现新版本
        $hasNew = false;
        if ($lastVersion) {
            $hasNew = version_compare($currentVersion, $lastVersion->version, '<');
        }

        $msg = $hasNew ? '发现新版本' : '已是最新版本';

        if ($hasNew) {
            $ret = [
                'has_new' => $hasNew,
                'new_version' => $lastVersion->version,
                'enforce' => !!$lastVersion->enforce,
                'download_url' => cdnurl($lastVersion->download_file, true),
                'content' => $lastVersion->content,
            ];
        }

        $this->success($msg, $ret);
    }

    /**
     * 修改个人信息
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/13 14:52
     */
    public function changeInfo()
    {
        $params = input('post.');

        if (isset($params['avatar_url'])) {
            $params['avatar'] = $params['avatar_url'];
        }
        unset($params['avatar_url']);

        if (empty($params)) {
            $this->error('未修改任何内容');
        }

        $rule = [
            'avatar' => 'min:1',
            'nickname' => 'min:1|max:30'
        ];
        $field = [
            'avatar' => '头像',
            'nickname' => '昵称'
        ];
        $validate = new Validate($rule, [], $field);
        $result = $validate->check($params);
        if (!$result) {
            $this->error($validate->getError());
        }

        $user = $this->auth->getUser();
        $user->allowField(array_keys($rule))->save($params);
        $this->success('保存成功');
    }

    /**
     * 检查发货信息
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function checkDeliveryInfo()
    {
        $prize_record_id = input('record_ids');
        if (empty($prize_record_id)) {
            $this->error('未选择盲盒商品');
        }

        $prizeIds = explode(',', trim($prize_record_id, ','));
        $prizeIds = array_unique($prizeIds);

        $orderInfo = [];
        $delivery_fee = 0;

        // 检查选择的每一个奖品
        foreach ($prizeIds as $prizeId) {
            if (!is_numeric($prizeId)) {
                $this->error('盲盒ID有误');
            }

            // 检查奖品是否有效
            $prize = Prizerecord::alias('prize')
                ->field('prize.id record_id,prize.goods_name,prize.goods_image,prize.delivery_fee,prize.create_time')
                ->field('order.coin_price box_coin_price,order.rmb_price box_rmb_price,order.pay_method')
                ->join('order order', 'order.id = prize.order_id')
                ->where('prize.user_id', $this->auth->id)
                ->where('prize.id', $prizeId)
                ->where('prize.status', 'bag') // 奖品状态:bag=盒柜,exchange=已回收,delivery=申请发货,received=已收货
                ->find();

            if (empty($prize)) {
                $this->error('选择的盲盒有误');
            }
            $prize->goods_image = $prize->goods_image ? cdnurl($prize->goods_image, true) : '';
            $prize->time = date('Y-m-d H:i:s', $prize->create_time);
            $delivery_fee += $prize->delivery_fee;
            $prize->delivery_fee = '￥' . $prize->delivery_fee;

            $pay_method_list = [
                'coin' => '金币',
                'wechat' => '微信',
                'alipay' => '支付宝',
                'money'=>'余额'
            ];

            if ('coin' == $prize->pay_method) {
                $prize->pay_amount = $prize->box_coin_price . '金币';
            } else {
                $prize->pay_amount = $prize->box_rmb_price . '元';
            }
           if($prize->pay_method ==NULL){
             $prize->pay_method = 'alipay';
              }
            $prize->pay_method = $pay_method_list[$prize->pay_method];
            unset($prize->box_rmb_price);
            unset($prize->box_coin_price);
            unset($prize->create_time);

            $orderInfo[] = $prize->toArray();
        }

        $ret = [
            'total_delivery_fee' => $delivery_fee,
            'order_info' => $orderInfo
        ];

        $this->success('查询成功', $ret);
    }

    /**
     * 申请发货
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function applyDelivery()
    {
        $address_id = input('address_id');
        $prize_record_id = input('record_ids');

        if (empty($address_id)) {
            $this->error('未选择收货地址');
        }

        if (empty($prize_record_id)) {
            $this->error('未选择盲盒商品');
        }

        // 检查收货地址
        $address = UserAddress::field('username,mobile,province,city,area,detail')
            ->where('id', $address_id)
            ->where('user_id', $this->auth->id)
            ->find();

        if (empty($address)) {
            $this->error('收货地址有误');
        }

        $prizeIds = explode(',', trim($prize_record_id, ','));
        $prizeIds = array_unique($prizeIds);

        $delivery_fee = 0;
        $trade = null;

        // 检查选择的每一个奖品
        foreach ($prizeIds as $prizeId) {
            if (!is_numeric($prizeId)) {
                $this->error('盲盒ID有误');
            }

            // 检查奖品是否有效
            $prize = Prizerecord::field('id,delivery_fee')
                ->where('id', $prizeId)
                ->where('user_id', $this->auth->id)
                ->where('status', 'bag') // 奖品状态:bag=盒柜,exchange=已回收,delivery=申请发货,received=已收货
                ->find();

            if (empty($prize)) {
                $this->error('选择的盲盒有误');
            }
            $delivery_fee += floatval($prize->delivery_fee);
        }
 


        // $delivery_fee  = 0.1;
        Db::startTrans();
        try {
            // 如果需要支付的话 订单为未支付状态 拉起支付  否则 改为待发货状态
            if($delivery_fee > 0){
                $deliveryStatus = 'unpay';
            }else{
                $deliveryStatus = 'paid';
            }
            
            // 新加记录用户积分日志
            $user= Db::table('box_user') ->where('id',$this->auth->id)->find(); 
            $yq=$user['coin'];
            $coinAmount = ceil(Setting::getCoinFromRmb(round($delivery_fee, 2)));
            $jq=$coinAmount;  // 增加的积分
            $hq=$yq+$jq;
             if($delivery_fee <= 0){
                Db::table('box_user_score_log')->insertGetId([
                      'user_id' => $user['id'],
                      'score' => $jq,
                      'before' => $yq,
                      'after' => $hq,
                      'memo' => '商品发货',
                      'createtime' =>time()
                    ]);
              }
            

            // 创建发货交易订单
            $trade = DeliveryTrade::create([
                'user_id' => $this->auth->id,
                'rmb_amount' => round($delivery_fee, 2),
                'coin_amount' => $coinAmount,
                'status' => $deliveryStatus,
                'out_trade_no' => date('YmdHis') . mt_rand(10000, 99999)
            ]);
           
            // 新加清空购物车

            foreach ($prizeIds as $prizeId) {
                $prize = Prizerecord::where('id', $prizeId)->find();
                //  [id] => 1992
            // [box_id] => 48
            // [order_id] => 1453
            // [out_trade_no] => 202201172320492762153
            // [user_id] => 191
            // [goods_id] => 308
            // [goods_name] => u盘32g高速usb
            // [goods_image] => http://h5mhsc.oss-cn-beijing.aliyuncs.com/d3c7b4b9f0bbc3717250d2e0e77d83fa.jpg
            // [goods_coin_price] => 45
            // [goods_rmb_price] => 11.25
            // [status] => bag
            // [exchange_time] => 
            // [delivery_time] => 
            // [delivery_fee] => 1.00
            // [create_time] => 1642432852
            // [update_time] => 1642432852
            // [delete_time] => 
            // [zz_time] => 0

                if($deliveryStatus == 'unpay'){
                    // 创建发货订单
                    Delivery::create([
                        'order_id' => $prize->order_id,
                        'out_trade_no' => $prize->out_trade_no,
                        'delivery_trade_id' => $trade->id,
                        'delivery_order_no' => date('YmdHis') . mt_rand(1000, 9999),
                        'prize_id' => $prize->id,
                        'goods_name' => $prize->goods_name,
                        'goods_image' => $prize->goods_image,
                        'user_id' => $this->auth->id,
                        'username' => $address->username,
                        'mobile' => $address->mobile,
                        'province' => $address->province,
                        'city' => $address->city,
                        'area' => $address->area,
                        'detail' => $address->detail,
                        'address' => $address->province . $address->city . $address->area . $address->detail,
                    ]);
                    
                }else{
                    Db::table('box_prize_record')->where('id', $prizeId)->setField('status','delivery');
                    
                     // 新加创建待发货订单
                    Delivery::create([
                        'order_id' => $prize->order_id,
                        'out_trade_no' => $prize->out_trade_no,
                        'delivery_trade_id' => $trade->id,
                        'delivery_order_no' => date('YmdHis') . mt_rand(1000, 9999),
                        'prize_id' => $prize->id,
                        'goods_name' => $prize->goods_name,
                        'goods_image' => $prize->goods_image,
                        'user_id' => $this->auth->id,
                        'username' => $address->username,
                        'mobile' => $address->mobile,
                        'province' => $address->province,
                        'city' => $address->city,
                        'area' => $address->area,
                        'detail' => $address->detail,
                        'address' => $address->province . $address->city . $address->area . $address->detail,
                        'status' => 'undelivered'  // 0运费改为待发货状态
                    ]);
                    
                }

            }
        } catch (\Exception $e) {
            Db::rollback();
            dta(['user_id', $this->auth->id, 'prize_ids' => $prizeIds, 'error' => $e->getMessage()], '用户申请发货失败');
            $this->error('申请发货失败');
        }

        Db::commit();
        $ret = [
            'order_id' => intval($trade->id),
            'alipay' => $this->request->domain() . '/api/alipay/deliverypay/orderid/' . intval($trade->id),
            'wechat' => '/api/wechat/deliverypay/orderid/' . intval($trade->id),
            'delivery_fee' => $delivery_fee
        ];
        $this->success('创建订单成功', $ret);
    }

    /**
     * 金币支付发货订单
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/22 17:12
     */
    public function coinPayDelivery()
    {
        $order_id = input('order_id/d');
        if (empty($order_id)) {
            $this->error('请选择支付订单');
        }

        $trade = DeliveryTrade::where('id', $order_id)->where('user_id', $this->auth->id)->find();

        if (empty($trade)) {
            $this->error('订单不存在');
        }

        if ('unpay' != $trade->status) {
            $this->error('该订单已支付，请勿重复支付');
        }

        // 查询用户余额
        if (intval($this->auth->coin) < $trade->coin_amount) {
            $this->error('您的金币不足');
        }

        Db::startTrans();

        try {
            // 更新订单信息
            $trade->pay_method = 'coin';
            $trade->pay_coin = $trade->coin_amount;
            $trade->pay_time = time();
            $trade->status = 'paid';// 状态:unpay=待支付,paid=已支付
            $trade->save();

            $coin_before = $this->auth->coin;

            // 减少金币余额
            $user = $this->auth->getUser();
            $user->setDec('coin', $trade->pay_coin);

            // 创建金币使用记录
            CoinRecord::create([
                'user_id' => $this->auth->id,
                'before' => $coin_before,
                'after' => $this->auth->coin,
                'coin' => -$trade->pay_coin,
                'order_id' => $trade->id,
                'type' => 'pay_delivery', // 变更类型:pay_box=支付盲盒,recharge=充值,fromwallet=余额转入,pay_delivery=支付运费
            ]);

            // 变更发货订单状态
            $deliveryOrder = Delivery::where('delivery_trade_id', $trade->id)->select();
            $prizeIds = [];
            foreach ($deliveryOrder as $order) {
                $order->save(['status' => 'undelivered']);
                $prizeIds[] = $order->prize_id;
            }

            // 变更奖品状态
            Prizerecord::whereIn('id', $prizeIds)->update(['status' => 'delivery', 'delivery_time' => time()]);

        } catch (\Exception $e) {
            Db::rollback();
            dta('用户支付失败，订单已回滚到待支付');
            $this->error('支付失败');
        }

        Db::commit();

        $this->success('支付成功');
    }

    /**
     * 盒机回收
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function exchange()
    {
        $prize_record_id = input('record_ids');
        if (empty($prize_record_id)) {
            $this->error('未选择盲盒商品');
        }

        $prizeIds = explode(',', trim($prize_record_id, ','));
        $prizeIds = array_unique($prizeIds);

        // 检查选择的每一个奖品
        foreach ($prizeIds as $prizeId) {
            if (!is_numeric($prizeId)) {
                $this->error('盲盒ID有误');
            }

            // 检查奖品是否有效
            $prize = Prizerecord::field('id')
                ->where('user_id', $this->auth->id)
                ->where('id', $prizeId)
                ->where('status', 'bag') // 奖品状态:bag=盒柜,exchange=已回收,delivery=申请发货,received=已收货
                ->find();

            if (empty($prize)) {
                $this->error('选择的盲盒有误');
            }
        }

        $total = 0;
        $prizeCount = [];
        $prizeInfo = [];

        Db::startTrans();
        try {

            $user = $this->auth->getUser();

            foreach ($prizeIds as $prizeId) {
                $prize = Prizerecord::where('id', $prizeId)->find();

                // 更新盒柜
                $prize->save(['status' => 'exchange', 'exchange_time' => time()]); // 奖品状态:bag=盒柜,exchange=已回收,delivery=申请发货,received=已收货

                // 回收前用户余额
                $money_before = $this->auth->money;

                // 查询回收折扣
                $recovery_discount = floatval(Setting::getSetting('recovery_discount'));

                // 计算折扣后金额
                $amount = floor($prize->goods_coin_price * round($recovery_discount / 100, 2));

                $total += $amount;

                // 统计回收信息
                if (empty($prizeCount[$prize->goods_id])) {
                    $prizeCount[$prize->goods_id] = 1;
                } else {
                    $prizeCount[$prize->goods_id]++;
                }

                $prizeInfo[$prize->goods_id] = [
                    'name' => $prize->goods_name,
                    'num' => $prizeCount[$prize->goods_id],
                    'price' => $amount
                ];

                // 增加余额
                $user->setInc('money', $amount);

                // 创建余额记录
                MoneyRecord::create([
                    'user_id' => $this->auth->id,
                    'before' => $money_before,
                    'after' => $this->auth->money,
                    'money' => $amount,
                    'prize_id' => $prize->id,
                    'type' => 'box_exchange', // 变更类型:box_exchange=盲盒回收,refund=库存不足，支付返回,withdrawal=提现,to_coin=转到钱包，withdrawal_fail=提现失败,pay_delivery=支付运费
                ]);
            }
        } catch (\Exception $e) {
            Db::rollback();
            dta(['user_id', $this->auth->id, 'prize_ids' => $prizeIds, 'error' => $e->getMessage()], '用户申请发货失败');
            $this->error('回收失败');
        }

        Db::commit();

        $ret = [
            'amount' => $total,
            'goods_info' => array_values($prizeInfo),
            'notice' => Text::getText('recovery_rule')
        ];

        $this->success('回收成功', $ret);
    }
    
      public function echargez()
    {
        $prizeId = input('record_ids');
        $phone = input('mobile');
        $time = time() - 86400;
            // 检查奖品是否有效
            $prize = Prizerecord::field('id')
                ->where('user_id', $this->auth->id)
                ->where('id', $prizeId)
                ->where('status', 'bag')
                ->where('zz_time','<', $time)
                // 奖品状态:bag=盒柜,exchange=已回收,delivery=申请发货,received=已收货
                ->find();
            if (empty($prize)) {
                $this->error('盲盒失效或者未满24小时无法进行操作！');
            }
            $users =  \app\common\model\User::where('mobile',$phone)->find();
            if($users){
                
            
            $mobile = $this->auth->mobile;
            
            $prize = Prizerecord::where('id', $prizeId)->find();

                // 更新盒柜
               
                
                if($phone == $mobile){
                     $this->error('不能转赠给自己!');
                }
                 $prize->save(['user_id'=>$users['id'],'zz_time' => time()]);
                 $times = date("Y-m-d H:i:s",time());
               $data = [
                    'boxid' => $prizeId,
                    'boxgoods' => $prize['goods_name'],
                    'zzuser' => $mobile,
                    'szuser' => $phone,
                    'zzid' => $this->auth->id,
                    'zhuanztime' => $times,
                    'zzimage'   =>$prize['goods_image']
                ];
             
if(db('zz')->insert($data)){
    $this->success('转增成功');
}else{
    $this->error('转增失效');
}}else{
    
    $this->error('手机号未注册！');
}
        
            
}
    
    
    public function echargezs()
    {
        $user =   $this->auth->id;
             
        $data = db('zz')->where('zzid', $user)->select();
        $this->success('转增成功',$data);
            
}
    
    
    
    
    
    
    
    
    

    /**
     * 搜索历史
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function searchHistory()
    {
        $list = SearchHistory::where('user_id', $this->auth->id)->column('search');
        $this->success('查询成功', ['history' => $list]);
    }

    /**
     * 清除搜索历史
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function clearHistory()
    {
        SearchHistory::where('user_id', $this->auth->id)->delete();
        $this->success('清除成功');
    }

    /**
     * 余额转出到金币
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/15 14:44
     */
    public function moneyToCoin()
    {
        $amount = input('amount/d');
        if (!is_numeric($amount) || empty($amount)) {
            $this->error('金额只能是大于0的整数');
        }

        Db::startTrans();
        try {
            $user = $this->auth->getUser();
            $coin_before = $this->auth->coin;
            $money_before = $this->auth->money;

            // 检查余额够不够
            if ($money_before < $amount) {
                Db::rollback();
                $this->error('余额不足');
            }

            // 增加金币
            $user->setInc('coin', intval($amount));

            // 创建金币记录
            CoinRecord::create([
                'user_id' => $this->auth->id,
                'before' => $coin_before,
                'after' => $this->auth->coin,
                'coin' => $amount,
                'type' => 'from_balance', // 变更类型:pay_box=支付盲盒,recharge=充值,from_balance=余额转入,refund=库存不足，支付返回,pay_delivery=支付运费
            ]);

            // 减少余额
            $user->setDec('money', intval($amount));

            // 创建余额记录
            MoneyRecord::create([
                'user_id' => $this->auth->id,
                'before' => $money_before,
                'after' => $this->auth->money,
                'money' => -$amount,
                'type' => 'to_coin', // 变更类型:box_exchange=盲盒回收,refund=库存不足，支付返回,withdrawal=提现,to_coin=转到钱包，withdrawal_fail=提现失败
            ]);

        } catch (\Exception $e) {
            Db::rollback();
            $this->error('转出失败');
        }

        Db::commit();

        $this->success('转出到钱包成功');
    }

    /**
     * 申请提现
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function withdrawal()
    {
        $amount = input('amount/d');
        $type = input('type');
        if (!is_numeric($amount) || empty($amount) || $amount < 10) {
            $this->error('金额有误最低提现10元');
        }

        if (!in_array($type, ['wechat', 'alipay'])) {
            $this->error('提现方式有误');
        }

        $user = $this->auth->getUser();

        if ('wechat' == $type) {
            if (empty($user->wechat_username) || empty($user->wechat_account)) {
                $this->error('请先绑定微信提现账户');
            }

            $username = $user->wechat_username;
            $account = $user->wechat_account;

        } else {
            if (empty($user->alipay_username) || empty($user->alipay_account)) {
                $this->error('请先绑定支付宝提现账户');
            }

            $username = $user->alipay_username;
            $account = $user->alipay_account;
        }

        Db::startTrans();
        try {
            $user = $this->auth->getUser();
            $money_before = $this->auth->retail_balance;

            // 检查余额够不够
            if ($money_before < $amount) {
                Db::rollback();
                $this->error('余额不足');
            }

            // 减少余额
            $user->setDec('retail_balance', intval($amount));

           // 创建余额记录
            // MoneyRecord::create([
            //     'user_id' => $this->auth->id,
            //     'before' => $money_before,
            //     'after' => $this->auth->retail_balance,
            //     'money' => -$amount,
            //     'type' => 'withdrawal', // 变更类型:box_exchange=盲盒回收,refund=库存不足，支付返回,withdrawal=提现,to_coin=转到钱包，withdrawal_fail=提现失败
            // ]);

            // 创建提现申请
            Withdrawal::create([
                'user_id' => $this->auth->id,
                'amount' => $amount,
                'type' => $type,
                'username' => $username,
                'account' => $account,
                'drawal_type' => 1,
            ]);

        } catch (\Exception $e) {
            Db::rollback();
            $this->error('佣金提现失败');
        }

        Db::commit();

        $this->success('佣金提现成功');
    }
     public function withdrawals()
    {
        $amount = input('amount/d');
        $type = input('type');
        if (!is_numeric($amount) || empty($amount) || $amount < 10) {
            $this->error('金额有误最低提现10元');
        }

        if (!in_array($type, ['wechat', 'alipay'])) {
            $this->error('提现方式有误');
        }

        $user = $this->auth->getUser();

        if ('wechat' == $type) {
            if (empty($user->wechat_username) || empty($user->wechat_account)) {
                $this->error('请先绑定微信提现账户');
            }

            $username = $user->wechat_username;
            $account = $user->wechat_account;

        } else {
            if (empty($user->alipay_username) || empty($user->alipay_account)) {
                $this->error('请先绑定支付宝提现账户');
            }

            $username = $user->alipay_username;
            $account = $user->alipay_account;
        }

        Db::startTrans();
        try {
            $user = $this->auth->getUser();
            $money_before = $this->auth->money;

            // 检查余额够不够
            if ($money_before < $amount) {
                Db::rollback();
                $this->error('余额不足');
            }

            // 减少余额
            $user->setDec('money', intval($amount));

          //  创建余额记录
            MoneyRecord::create([
                'user_id' => $this->auth->id,
                'before' => $money_before,
                'after' => $this->auth->money,
                'money' => -$amount,
                'type' => 'withdrawals', // 变更类型:box_exchange=盲盒回收,refund=库存不足，支付返回,withdrawal=提现,to_coin=转到钱包，withdrawal_fail=提现失败
            ]);

            // 创建提现申请
            Withdrawal::create([
                'user_id' => $this->auth->id,
                'amount' => $amount,
                'type' => $type,
                'username' => $username,
                'account' => $account,
                'drawal_type' => 0,
            ]);

        } catch (\Exception $e) {
            Db::rollback();
            $this->error('余额提现失败');
        }

        Db::commit();

        $this->success('余额提现成功');
    }

    /**
     * 获取提现绑定信息
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function getWithdrawalSetting()
    {
        $user = $this->auth->getUser();
        $ret = [
            'wechat' => [
                'username' => $user->wechat_username ?: '',
                'account' => $user->wechat_account ?: ''
            ],
            'alipay' => [
                'username' => $user->alipay_username ?: '',
                'account' => $user->alipay_account ?: ''
            ]
        ];

        $this->success('查询成功', $ret);
    }

    /**
     * 获取提现绑定信息
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function bindWithdrawalAccount()
    {
        $type = input('type');
        $username = input('username');
        $account = input('account');
        if (!in_array($type, ['wechat', 'alipay'])) {
            $this->error('绑定类型有误');
        }

        if (empty($username) || empty($account)) {
            $this->error('姓名和账户不能为空');
        }

        if ('wechat' == $type) {
            $update = [
                'wechat_username' => $username,
                'wechat_account' => $account
            ];
        } else {
            $update = [
                'alipay_username' => $username,
                'alipay_account' => $account
            ];
        }

        $user = $this->auth->getUser();
        try {
            $user->save($update);
        } catch (Exception $e) {
            $this->error('信息有误');
        }

        $this->success('綁定成功');
    }

    /**
     * 快递、物流信息
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public function express()
    {
        $order_id = input('delivery_order_id');
        if (empty($order_id)) {
            $this->error('未选择订单');
        }

        $list = [];
        $order = Delivery::field('delivery_number,post_name,post_code')
            ->where('id', $order_id)
            ->where('user_id', $this->auth->id)
            ->where('status', 'neq', 'undelivered') // 发货状态:undelivered=待发货,unreceived=待收货,finished=已完成
            ->find();

        if (empty($order)) {
            $this->success('未查询到快递信息', ['list' => $list]);
        }

        // TODO 对接第三方物流接口
        //  ...

        $list = [
            [
                'time' => '2018-10-13 13:01:10',
                'desc' => '您已签收本次订单的包裹，本次配送完成'
            ],
            [
                'time' => '2018-10-13 10:01:10',
                'desc' => '快递到达某某地'
            ],
            [
                'time' => '2018-10-13 10:01:10',
                'desc' => '快递到达某某地'
            ],
            [
                'time' => '2018-10-13 10:01:10',
                'desc' => '快递到达某某地'
            ],
        ];

        $this->success('查询成功', ['list' => $list]);
    }
}
