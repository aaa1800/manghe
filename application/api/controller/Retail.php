<?php

namespace app\api\controller;

use think\Db;
use think\Request;
use app\common\controller\Api;

/**
 * 
 */
class Retail  extends Api
{   
    protected $noNeedRight = ['*'];


    public function getTotalCoin ()
    {
        $sum = db("retail_detail")->where(['user_id'=>$this->auth->id])->field("SUM(coin) total")->find();
        $dw = db("withdrawal")->where(['user_id'=>$this->auth->id,"status"=>"success"])->field("ifnull(SUM(amount),0) total")->find();
        $this->success("获取成功", ['total'=>$sum['total'], "dw"=>$dw['total']]);
    }

    /**
     * 明细
     * @Author   Nilr
     * @DateTime 2021-10-20T21:28:56+0800
     * @param    Request                  $request [description]
     * @return   [type]                            [description]
     */
	public function getRetailList(Request $request)
    {
        $page = $request->param("page", 1);
        $limit = $request->param("limit", 10);
        $list = db("retail_detail")->alias("a")->join("user b","b.id = a.source_user_id")->where(['user_id'=>$this->auth->id])->field("FROM_UNIXTIME(a.create_time,'%Y-%m-%d %H:%i:%s') create_time, avatar, b.nickname, a.level, a.coin")->order("a.id desc")->page($page, $limit)->select();
        // IF(b.avatar, b.avatar,'/default.png') 
        $this->success("获取成功", $list);
    }

    /**
     * 团队
     * @Author   Nilr
     * @DateTime 2021-10-20T21:29:02+0800
     * @param    Request                  $request [description]
     * @return   [type]                            [description]
     */
    public function getTeamList(Request $request)
    {
        $page = $request->param("page", 1);
        $limit = $request->param("limit", 10);
        $list = db("retail_invitation")->alias("a")->join("user b","b.id = a.user_id")->where(['a.pid'=>$this->auth->id])->field("FROM_UNIXTIME(a.create_time,'%Y-%m-%d %H:%i') create_time, b.username, a.level")->order("a.id desc")->page($page, $limit)->select();
        $this->success('获取成功', $list);
    }
    
    /**
     * 获取提现
     * 
     */
    public function getTixianList(Request $request)
    {
        $page = $request->param("page", 1);
        $limit = $request->param("limit", 10);
        $list = db("withdrawal")
                ->where(['user_id'=>$this->auth->id])
                ->page($page, $limit)
                ->select();
        foreach($list as &$v){
        $v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
        }
        $this->success('获取成功', $list);
    }
}
