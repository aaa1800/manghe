<?php

namespace app\api\controller;

use app\common\controller\Api;

/**
 * 盲盒接口
 */
class Box extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];


    /**
     * 获取盲盒底部图片
    */
    public function getFootImages(){
        $list = db('box')->where('switch','1')->where('box_foot_images','neq','')->column('box_foot_images');
        $arr = [];
        foreach ($list as $v){
            if (strstr($v,',')) {
                $arr = array_merge($arr,explode(',',$v));
            } else {
                $arr[] = $v;
            }
            
        }
        $this->success('查询成功',$arr);
    }
    
}
