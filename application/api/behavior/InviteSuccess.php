<?php

namespace app\api\behavior;

use app\api\model\InviteRecord;
use app\common\model\User;

/**
 * 创建邀请记录和邀请奖励
 * Class InviteSuccess
 * @package app\admin\behavior
 * @author fuyelk <fuyelk@fuyelk.com>
 */
class InviteSuccess
{
    public function run(&$params)
    {
        if (empty($params['id']) || empty($params['pid']) || $params['id'] == $params['pid']) {
            return false;
        }

        // 检查是否重复记录
        $exist = InviteRecord::where('user_id', $params['id'])->value('id');
        if ($exist) {
            return false;
        }

        // 检查邀请者
        $pid = User::where('id', $params['pid'])->value('id');
        if (!$pid) {
            return false;
        }

        // TODO 可以在这里添加邀请奖励

        InviteRecord::create([
            'user_id' => $params['id'],
            'inviter_id' => $params['pid']
        ]);
        return true;
    }
}
