<?php

namespace app\api\model;

use think\Model;


class RechargeList extends Model
{
    // 表名
    protected $name = 'recharge_list';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [];

}
