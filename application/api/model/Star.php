<?php

namespace app\api\model;

use think\Model;

class Star extends Model
{

    // 表名
    protected $name = 'star';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [];

    /**
     * 点击点赞按钮，点赞或取消点赞
     * @param int $user_id 用户ID
     * @param int $box_id 盲盒ID
     * @return int
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public static function click($user_id, $box_id)
    {
        $param = [
            'user_id' => $user_id,
            'box_id' => $box_id
        ];
        $row = self::field('id')->where($param)->find();
        if ($row) {
            $row->delete();
            return 0;
        }
        self::create($param);
        return 1;
    }

    /**
     * 检查是否已点赞
     * @param int $user_id 用户ID
     * @param int $box_id 盲盒ID
     * @return int
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public static function check($user_id, $box_id)
    {
        $param = [
            'user_id' => $user_id,
            'box_id' => $box_id
        ];
        $row = self::field('id')->where($param)->find();
        return $row ? 1 : 0;
    }
}
