<?php

namespace app\api\model;

use think\Model;
use traits\model\SoftDelete;


class Version extends Model
{

    use SoftDelete;

    // 表名
    protected $name = 'app_version';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';

    // 追加属性
    protected $append = [];

    /**
     * 获取最新版本信息
     * @param string $version 当前版本号
     * @param string $platform 系统记录的最新版本号
     * @return array|bool|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     */
    public static function getLatest($version, $platform)
    {
        $lastVersion = self::field('version,content,download_file,enforce')->where('platform', $platform)->order('id', 'desc')->find();
        if (empty($lastVersion)) {
            return false;
        }

        return $lastVersion;
    }

}
