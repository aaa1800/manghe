<?php

namespace app\api\model;

use think\Model;


class SearchHistory extends Model
{

    // 表名
    protected $name = 'search_history';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [];

}
