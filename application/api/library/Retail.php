<?php

namespace app\api\library;

use app\api\model\CoinRecord;
use think\Db;

/**
 * 分销
 */
class Retail
{
	/**
	 * 计算佣金
	 * @Author   Nilr
	 * @DateTime 2021-10-20T19:55:34+0800
	 * @return   [type]                   [description]
	 */
    public static function giveMoney ($order) {
        $order = \app\api\model\RechargeOrder::get($order['id']);
    	if($order->status != "paid") return false;
    	$user_id = $order->user_id;  //购买的用户id
    	$money = $order->rmb_amount;  //支付金额
    	// 匹配往上3级的用户
    	$f_relation = db("retail_invitation")->alias("a")->join("user b","b.id = a.pid")->where(['a.user_id'=>$user_id])->field("b.id, b.retail_1, b.retail_2, b.retail_3, a.level")->select();
    	//比例
    	// var_dump($f_relation);
    	$system = db("setting")->where(['id'=>1])->field("retail_1, retail_2, retail_3")->find();
    	$arr = ['retail_1', "retail_2", "retail_3"];
    	foreach($f_relation as $k=>$v){
    		$v['now_level'] = $v[$arr[$v['level']-1]] > 0 ? $v[$arr[$v['level']-1]] : $system[$arr[$v['level']-1]];
    		self::sendCoin($v, $money, $order, $v['level']);
    	}
    }
     public static function giveMoneys ($order) {
        $order = \app\api\model\Order::get($order['id']);
        
      
    	if($order->status != "unused") return false;
    	$user_id = $order->user_id;  //购买的用户id
    	$money = $order->rmb_amount;  //支付金额
    	// 匹配往上3级的用户
    	$f_relation = db("retail_invitation")->alias("a")->join("user b","b.id = a.pid")->where(['a.user_id'=>$user_id])->field("b.id, b.retail_1, b.retail_2, b.retail_3, a.level")->select();
    	//比例
    	// var_dump($f_relation);
    	$system = db("setting")->where(['id'=>1])->field("retail_1, retail_2, retail_3")->find();
    	$arr = ['retail_1', "retail_2", "retail_3"];
    	foreach($f_relation as $k=>$v){
    		$v['now_level'] = $v[$arr[$v['level']-1]] > 0 ? $v[$arr[$v['level']-1]] : $system[$arr[$v['level']-1]];
    		self::sendCoin($v, $money, $order, $v['level']);
    	}
    }
    private static function sendCoin($v, $money, $order, $level = 1)
    {
    	$user = \app\common\model\User::get($v['id']);
    	if(empty($user)) return false;
        Db::startTrans();
        
               
        
        
        
    	$before = $user->retail_balance;
    	$moneyAfter = $money * $v['now_level'] / 100;
    	$after = $before + $moneyAfter;
    	// 创建金币记录
        // CoinRecord::create([
        //     'user_id' => $v['id'],
        //     'before' => $before,
        //     'after' => $after,
        //     'coin' => $moneyAfter,
        //     'order_id' => $order->id,
        //     'type' => 'retail', // 变更类型:pay_box=支付盲盒,recharge=充值,fromwallet=余额转入,refund=退款，retail分佣
        // ]);
        $user->retail_balance = $after;
        $sourcephone = \app\common\model\User::get($order->user_id);
        
        $arr = [
        	"user_id"=>$v['id'],
        	"source_user_id" =>$order->user_id,
        	'userphone'=>$user->username,
        	'sourcephone'=>$sourcephone->username,
        	"coin"=>$moneyAfter,
        	"level"=>$level,
        	"create_time"=>time()
        ];
        $res2 = db("retail_detail")->insert($arr);
        $res = $user->save();
        if($res && $res2){
        	Db::commit();  
        	return true;
        }
        Db::rollback();
        return false;
    }


    public static function bindRelation($pid, $uid){
    	if(empty($pid)) return true;
    	//找到pid 的上级
    	$f_relation = db("retail_invitation")->where(['user_id'=>$pid])->where("level", "in", "1,2")->select();
    	$arr = [];
    	foreach($f_relation as $k=>&$v){
    		$v['user_id'] = $uid;
    		$v['level'] = $v['level'] + 1;
    		unset($v['id']);
    		$arr[] = $v;
    	}
    	$arr[] = [
    		"user_id"=>$uid,
    		"pid"=>$pid,
    		"level"=>1,
    		"create_time"=>time()
    	];
    	if(count($arr) > 0) db("retail_invitation")->insertAll($arr);
    	return true;
    }
}
