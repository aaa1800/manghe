<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Config;
use think\Route;
use think\Url;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        // $order = \app\api\model\RechargeOrder::get(246);
        // \app\api\library\Retail::giveMoney($order);
        // echo(1);die;
        Header("Location: /h5"); die;
        return $this->view->fetch();
    }

    /**
     * 将用户协议伪装为静态文本
     * @param string $name
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author fuyelk <fuyelk@fuyelk.com>
     * @date 2021/07/05 19:40
     */
    public function agreement($name = '')
    {
        if (empty($name)) {
            echo "";
            exit();
        }
        $data = db('text')->field('desc,text')->where('title', $name)->where('public', 1)->find();
        if (empty($data)) {
            echo "";
            exit();
        }
        $html = <<<EOF
<!doctype html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"><title>{$data['desc']}</title></head><body>{$data['text']}</body></html>
EOF;
        echo $html;

    }

}
