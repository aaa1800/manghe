<?php
// +-------------------------------------------
// | redis配置
// +-------------------------------------------
return array (
  'host' => '127.0.0.1',
  'port' => 6379,
  'password' => '123456',
  'select' => 0,
  'timeout' => 0,
  'expire' => 0,
  'persistent' => false,
  'prefix' => 'box_',
  'serialize' => true,
);